package de.hdm.timemachine.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import de.hdm.timemachine.factories.DateFactory;
import de.hdm.timemachine.managers.RecordManager;
import de.hdm.timemachine.models.Record;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

public class RecordManagerTest {
    private RecordManager recordManagerSpy;
    private DateFactory dateFactoryMock;
    private Date currentDate;

    /**
     * Setup up the test environment.
     */
    @Before
    public void initialize() {
        this.currentDate = new Date();
        dateFactoryMock = mock(DateFactory.class);
        doReturn(this.currentDate).when(dateFactoryMock).getDate();
        recordManagerSpy = spy(new RecordManager(dateFactoryMock));
    }

    @Test
    public void startRecord() {
        Record record = new Record();
        recordManagerSpy.startRecord(record);
        Date expected = this.currentDate;
        Date actual = record.getStart();
        assertEquals(expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void startRecordNull() {
        recordManagerSpy.startRecord(null);
    }

    @Test
    public void stopRecord() {
        Record record = new Record();
        recordManagerSpy.stopRecord(record);
        Date expected = this.currentDate;
        Date actual = record.getEnd();
        assertEquals(expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void stopRecordNull() {
        recordManagerSpy.startRecord(null);
    }

    @Test
    public void startAndStopRecord() {
        Record record = new Record();
        recordManagerSpy.startRecord(record);
        recordManagerSpy.stopRecord(record);
        Date expected = this.currentDate;
        Date actual = record.getStart();
        assertEquals(expected, actual);
    }

    @Test
    public void stopAndStartRecord() {
        Record record = new Record();
        recordManagerSpy.stopRecord(record);
        recordManagerSpy.startRecord(record);
        Date expected = this.currentDate;
        Date actual = record.getStart();
        assertEquals(expected, actual);
    }

    @Test
    public void startRecordTwice() {
        Record record = new Record();
        recordManagerSpy.startRecord(record);
        recordManagerSpy.startRecord(record);
        Date expected = this.currentDate;
        Date actual = record.getStart();
        assertEquals(expected, actual);
    }

    @Test
    public void stopRecordTwice() {
        Record record = new Record();
        recordManagerSpy.stopRecord(record);
        recordManagerSpy.stopRecord(record);
        Date expected = this.currentDate;
        Date actual = record.getEnd();
        assertEquals(expected, actual);
    }

    @Test
    public void getDurationRecordNull() {
        long expected = 0;
        long actual = recordManagerSpy.getDuration(null);
        assertEquals(expected, actual);
    }

    @Test
    public void getDurationRecordNotStarted() {
        Record record = new Record();
        long expected = 0;
        long actual = recordManagerSpy.getDuration(record);
        assertEquals(expected, actual);
    }

    @Test
    public void getDurationRecordWithoutEnd() {
        long expected = 1000;
        Record record = new Record();
        record.setStart(this.currentDate);
        doReturn(this.currentDate.getTime() + expected).when(recordManagerSpy)
                .getDurationEndTime(record);
        long actual = recordManagerSpy.getDuration(record);
        assertEquals(expected, actual);
    }

    @Test
    public void getDurationRecordWithEnd() {
        long expected = 1000;
        Date endDate = new Date(this.currentDate.getTime() + expected);
        Record record = new Record();
        record.setStart(this.currentDate);
        record.setEnd(endDate);
        long actual = recordManagerSpy.getDuration(record);
        assertEquals(expected, actual);
    }

    @Test
    public void getDurationEndTimeRecordNull() {
        long expected = 0;
        long actual = recordManagerSpy.getDurationEndTime(null);
        assertEquals(expected, actual);
    }

    @Test
    public void getDurationEndTimeRecordWithoutEnd() {
        Record record = new Record();
        long expected = this.currentDate.getTime();
        long actual = recordManagerSpy.getDurationEndTime(record);
        assertEquals(expected, actual);
    }

    @Test
    public void getDurationEndTimeRecordWithEnd() {
        Record record = new Record();
        Date endDate = new Date();
        record.setEnd(endDate);
        long expected = endDate.getTime();
        long actual = recordManagerSpy.getDurationEndTime(record);
        assertEquals(expected, actual);
    }
}

