package de.hdm.timemachine.misc;

import com.github.mikephil.charting.data.PieEntry;

import de.hdm.timemachine.factories.PieChartDataFactory;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.models.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PieChartDataHelper {

    private TaskManager taskManager;
    private PieChartDataFactory pieChartDataFactory;

    public PieChartDataHelper(TaskManager taskManager, PieChartDataFactory pieChartDataFactory) {
        this.pieChartDataFactory = pieChartDataFactory;
        this.taskManager = taskManager;
    }

    /**
     * Generates Entries for PieChart.
     *
     * @param tasks    List of Tasks
     * @param dateFrom Date from
     * @param dateTo   Date to
     * @return list of entries
     */
    public List<PieEntry> getEntries(List<Task> tasks, Date dateFrom, Date dateTo) {
        List<PieEntry> entries = new ArrayList<>();
        if (tasks == null) {
            return entries;
        }
        for (Task task : tasks) {
            if (!task.isValid()) {
                continue;
            }

            long duration = this.taskManager.getDurationInTimespan(task, dateFrom, dateTo);
            if (duration == 0) {
                continue;
            }
            PieEntry entry = this.pieChartDataFactory.createPieEntry(duration, task.getName());

            entries.add(entry);
        }
        return entries;
    }
}
