package de.hdm.timemachine.matchers;

import de.hdm.timemachine.models.Task;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

/**
 * A matcher that matches {@link de.hdm.timemachine.models.Task}s based on the given name.
 */
public class TaskNameMatcher extends BaseMatcher<Task> {
    private final String name;

    private TaskNameMatcher(String name) {
        this.name = name;
    }

    @Override
    public boolean matches(Object item) {
        return item instanceof Task
                && ((Task) item).isValid()
                && ((Task) item).getName().equals(name);
    }

    @Override
    public void describeTo(Description description) {

    }

    public static TaskNameMatcher taskNameIs(String name) {
        return new TaskNameMatcher(name);
    }
}
