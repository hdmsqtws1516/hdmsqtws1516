package de.hdm.timemachine.events;

import de.hdm.timemachine.models.Task;

public class TaskRemovedEvent {
    private final Task task;

    public TaskRemovedEvent(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return task;
    }
}