package de.hdm.timemachine;

import android.util.Log;
import io.realm.Realm;

public aspect TransactionAspect {
    private static final String LOG_TAG = "TransactionalAspect";

    pointcut transactional(): execution(@de.hdm.timemachine.Transactional * *(..));

    before(): transactional() {
        Realm realm = Realm.getDefaultInstance();

        if(realm.isInTransaction()) {
            Log.d(LOG_TAG, "already in transaction");
            return;
        }
        Log.d(LOG_TAG, "starting transaction");
        realm.beginTransaction();
    }

    after(): transactional() {
        Log.d(LOG_TAG, "committing transaction");
        Realm.getDefaultInstance().commitTransaction();
    }
}