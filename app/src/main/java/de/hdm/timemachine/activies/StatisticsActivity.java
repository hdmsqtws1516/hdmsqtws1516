package de.hdm.timemachine.activies;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.github.mikephil.charting.charts.PieChart;

import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import de.hdm.timemachine.R;
import de.hdm.timemachine.TaskList;
import de.hdm.timemachine.TimeMachineApplication;
import de.hdm.timemachine.factories.PieChartDataFactory;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.misc.PieChartDataHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class StatisticsActivity extends AppCompatActivity {
    private static final String KEY_TARGET = "target";

    private static final int TARGET_DATE_FROM = 1;
    private static final int TARGET_DATE_TO = 2;

    @BindView(R.id.chart) PieChart chart;
    @BindView(R.id.et_from) EditText etFrom;
    @BindView(R.id.et_to) EditText etTo;

    private TaskManager taskManager;
    private TaskList taskList;
    private Date dateFrom;
    private Date dateTo;

    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.GERMAN);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        ButterKnife.bind(this);

        TimeMachineApplication app = (TimeMachineApplication)getApplication();
        taskList = app.getTaskList();
        taskManager = app.getTaskManager();

        etFrom.setOnClickListener(datePickerClickListener(this, TARGET_DATE_FROM));

        etTo.setOnClickListener(datePickerClickListener(this, TARGET_DATE_TO));

        this.chart.setDescription(getString(R.string.time_wasted));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.loadPieChart();
    }

    private void loadPieChart() {
        PieChartDataFactory pcdf = new PieChartDataFactory();
        PieChartDataHelper pcd = new PieChartDataHelper(this.taskManager, pcdf);

        List<PieEntry> entries = pcd.getEntries(taskList.getAllTasks(), dateFrom, dateTo);
        PieDataSet dataSet = pcdf.createPieDataSet("Election Results",
                entries, this.getChartColors());
        PieData data = pcdf.createPieData(dataSet);
        data.setValueTextSize(15f);

        this.chart.setData(data);
        this.chart.invalidate();
    }

    /**
     * Get Pie Chart Colors.
     *
     * @return List of Colors
     */
    public ArrayList<Integer> getChartColors() {
        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS) {
            colors.add(c);
        }
        for (int c : ColorTemplate.JOYFUL_COLORS) {
            colors.add(c);
        }
        for (int c : ColorTemplate.COLORFUL_COLORS) {
            colors.add(c);
        }
        for (int c : ColorTemplate.LIBERTY_COLORS) {
            colors.add(c);
        }
        for (int c : ColorTemplate.PASTEL_COLORS) {
            colors.add(c);
        }

        colors.add(ColorTemplate.getHoloBlue());
        return colors;
    }

    private void onDateSet(int target, Date date) {
        String formattedDate = dateFormat.format(date);

        switch ( target ) {
            case TARGET_DATE_FROM:
                dateFrom = date;
                etFrom.setText(formattedDate);
                break;
            case TARGET_DATE_TO:
                dateTo = date;
                etTo.setText(formattedDate);
                break;
            default: break;
        }

        loadPieChart();
    }

    private static View.OnClickListener datePickerClickListener(
            final AppCompatActivity context, final int target) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                Bundle args = new Bundle();
                args.putInt(KEY_TARGET, target);
                newFragment.setArguments(args);
                newFragment.show(context.getSupportFragmentManager(), "datePicker");
            }
        };
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);

            ((StatisticsActivity)getActivity()).onDateSet(
                    getArguments().getInt(KEY_TARGET),
                    calendar.getTime()
            );
        }
    }
}

