package de.hdm.timemachine.integration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hdm.timemachine.factories.DateFactory;
import de.hdm.timemachine.factories.RecordFactory;
import de.hdm.timemachine.managers.RecordManager;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.models.Record;
import de.hdm.timemachine.models.Task;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

public class Taskmanager {
    private TaskManager taskManagerSpy;
    private RecordManager recordManagerSpy;
    private Date currentDate;
    private DateFactory dateFactoryMock;
    private Task taskSpy;
    private RecordFactory recordFactoryMock;

    /**
     * Setup up the test environment.
     */
    @Before
    public void setup() {
        this.currentDate = new Date();
        dateFactoryMock = mock(DateFactory.class);
        doReturn(this.currentDate).when(dateFactoryMock).getDate();
        recordManagerSpy = spy(new RecordManager(dateFactoryMock));
        recordFactoryMock = mock(RecordFactory.class);
        when(recordFactoryMock.getRecord()).thenReturn(spy(new Record()));
        taskManagerSpy = spy(new TaskManager(recordManagerSpy, recordFactoryMock));
        taskSpy = spy(new Task("TestTask"));
    }

    @Test
    public void getOverallDuration_4Records() {
        Long[] durations = {5556541242L, 1322334242L, 13254312L, 13412L};
        ArrayList<Record> records = new ArrayList<Record>();
        long durationSum = 0;
        for (int i = 0; i < durations.length; i++) {
            Record recSpy = new Record();
            doReturn(durations[i]).when(recordManagerSpy).getDuration(recSpy);
            records.add(recSpy);
            durationSum = durationSum + durations[i];
        }
        doReturn(records).when(taskSpy).getRecords();
        assertEquals(durationSum, taskManagerSpy.getOverallDuration(taskSpy));
    }

    @Test
    public void getOverallDuration_0Records() {
        assertEquals(0, taskManagerSpy.getOverallDuration(new Task()));
    }

    @Test
    public void isStartRecordCalledFromTaskManager() {
        taskManagerSpy.startTask(taskSpy);
        verify(recordManagerSpy, times(1)).startRecord(taskSpy.getActiveRecord());
    }

    @Test
    public void isStopRecordCalledFromTaskManager() {
        taskManagerSpy.startTask(taskSpy);
        Record rec = taskSpy.getActiveRecord();
        taskManagerSpy.stopTask(taskSpy);
        verify(recordManagerSpy, times(1)).stopRecord(rec);
    }

    @Test
    public void startTask_isStartDateInRecord() {
        doReturn(false).when(taskSpy).isOn();
        taskManagerSpy.startTask(taskSpy);
        assertEquals(currentDate, taskSpy.getActiveRecord().getStart());
    }

    @Test
    public void startTask_isStopDateInRecord() {
        taskManagerSpy.startTask(taskSpy);
        Record rec = taskSpy.getActiveRecord();
        taskManagerSpy.stopTask(taskSpy);
        assertEquals(currentDate, rec.getEnd());
    }

    @Test
    public void singleDurationInTimespan() {
        taskManagerSpy.startTask(taskSpy);
        Record rec = taskSpy.getActiveRecord();
        taskManagerSpy.stopTask(taskSpy);
        when(rec.getStart()).thenReturn(new Date(1400));
        when(rec.getEnd()).thenReturn(new Date(1700));
        long dur = taskManagerSpy.getDurationInTimespan(taskSpy, new Date(1000), new Date(3000));
        assertEquals(300, dur);
    }

    @Test
    public void singleDurationOutOfTimespan() {
        taskManagerSpy.startTask(taskSpy);
        Record rec = taskSpy.getActiveRecord();
        taskManagerSpy.stopTask(taskSpy);
        when(rec.getStart()).thenReturn(new Date(1400));
        when(rec.getEnd()).thenReturn(new Date(1700));
        long dur = taskManagerSpy.getDurationInTimespan(taskSpy, new Date(1000), new Date(1100));
        assertEquals(0, dur);
    }

    @Test
    @Ignore
    public void singleDurationInPartsOfTimespan() {
        taskManagerSpy.startTask(taskSpy);
        Record rec = taskSpy.getActiveRecord();
        taskManagerSpy.stopTask(taskSpy);
        when(rec.getStart()).thenReturn(new Date(400));
        when(rec.getEnd()).thenReturn(new Date(1300));
        long dur = taskManagerSpy.getDurationInTimespan(taskSpy, new Date(1000), new Date(1500));
        assertEquals(300, dur);
    }


}