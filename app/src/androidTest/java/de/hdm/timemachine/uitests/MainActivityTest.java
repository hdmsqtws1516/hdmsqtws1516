package de.hdm.timemachine.uitests;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static de.hdm.timemachine.matchers.TaskNameMatcher.taskNameIs;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import de.hdm.timemachine.R;
import de.hdm.timemachine.TimeMachineApplication;
import de.hdm.timemachine.Transactional;
import de.hdm.timemachine.activies.AddTaskActivity;
import de.hdm.timemachine.activies.MainActivity;
import de.hdm.timemachine.activies.StatisticsActivity;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented UI-tests for the {@link MainActivity}.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    /**
     * Clean up the database after each test.
     */
    @After
    @Transactional
    public void cleanUp() {
        TimeMachineApplication application = (TimeMachineApplication) getInstrumentation()
                .getTargetContext().getApplicationContext();
        application.getDatabaseRealm().deleteAll();
    }

    @Rule
    public IntentsTestRule<MainActivity> mainActivityRule =
            new IntentsTestRule<>(MainActivity.class);

    @Test
    public void actionButtonLoadsAddTaskActivity() {
        onView(ViewMatchers.withId(R.id.fab_create_task)).perform(click());
        intended(hasComponent(AddTaskActivity.class.getName()));
    }

    @Test
    public void addTask() {
        String taskName = "Lesen";
        onView(withId(R.id.fab_create_task)).perform(click());
        onView(withId(R.id.name)).perform(typeText(taskName));
        onView(withId(R.id.bn_create_task)).perform(click());
        onData(taskNameIs(taskName)).check(matches(notNullValue()));
    }

    @Test
    public void navigationDrawerDisplaysStatisticsButton() {
        onView(withText("Statistics")).check(matches(not(isDisplayed())));
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withText("Statistics")).check(matches(isDisplayed()));
    }

    @Test
    public void statisticsNavigationItemLoadsStatisticsActivity() {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withText("Statistics")).perform(click());
        intended(hasComponent(StatisticsActivity.class.getName()));
    }
}
