package de.hdm.timemachine.unit;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hdm.timemachine.factories.RecordFactory;
import de.hdm.timemachine.managers.RecordManager;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.models.Record;
import de.hdm.timemachine.models.Task;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

public class TaskManagerTest {
    private TaskManager taskManagerSpy;
    private RecordManager recordManagerMock = mock(RecordManager.class);
    private Task taskSpy;
    private Calendar calendar;
    private RecordFactory recordFactoryMock;

    /**
     * Setup up the test environment.
     */
    @Before
    public void setup() {
        recordFactoryMock = mock(RecordFactory.class);
        when(recordFactoryMock.getRecord()).thenReturn(new Record());
        taskManagerSpy = spy(new TaskManager(recordManagerMock, recordFactoryMock));
        taskSpy = spy(new Task("TestTask"));
        calendar = Calendar.getInstance();
    }

    @Test
    public void taskConstructorEmpty() {
        Task task = new Task();
        assertEquals(null, task.getName());
        assertEquals(false, task.isOn());
    }

    @Test
    public void hasRecordsTaskNull() {
        assertEquals(false, taskManagerSpy.hasRecords(null));
    }

    @Test
    public void hasRecordsTaskWithoutRecords() {
        assertEquals(false, taskManagerSpy.hasRecords(new Task()));
    }

    @Test
    public void hasRecordsTaskWithOneRecord() {
        Task task = new Task();
        taskManagerSpy.addRecordToTask(task, new Record());
        assertEquals(true, taskManagerSpy.hasRecords(task));
    }

    @Test
    public void hasRecordsTaskWithManyRecords() {
        Record record = mock(Record.class);
        Task task = this.getTaskWithRecords(100, 0);
        taskManagerSpy.addRecordToTask(task, record);
        assertEquals(true, taskManagerSpy.hasRecords(task));
    }

    @Test(expected = NullPointerException.class)
    public void addRecordToTaskNull() {
        taskManagerSpy.addRecordToTask(null, null);
    }

    @Test
    public void addRecordToTaskRecordNull() {
        Task task = new Task();
        taskManagerSpy.addRecordToTask(new Task(), null);
        assertEquals(0, task.getRecords().size());
    }

    @Test
    public void addRecordToTask() {
        Record record = mock(Record.class);
        taskManagerSpy.addRecordToTask(taskSpy, record);
        assertThat(taskSpy.getRecords(), contains(record));
    }

    @Test(expected = NullPointerException.class)
    public void startTaskNull() {
        taskManagerSpy.startTask(null);
    }

    @Test
    public void startTask() {
        taskManagerSpy.startTask(taskSpy);
        assertEquals(true, taskSpy.isOn());
        assertEquals(1, taskSpy.getRecords().size());
        verify(recordManagerMock, times(1)).startRecord(any(Record.class));
    }

    @Test
    public void startTaskWithRecordInList() {
        Record record = mock(Record.class);
        taskSpy.addRecord(record);
        taskManagerSpy.startTask(taskSpy);
        assertEquals(true, taskSpy.isOn());
        assertEquals(2, taskSpy.getRecords().size());
    }

    @Test
    public void startTaskTwice() {
        taskManagerSpy.startTask(taskSpy);
        taskManagerSpy.startTask(taskSpy);
        assertEquals(1, taskSpy.getRecords().size());
        assertEquals(true, taskSpy.isOn());
    }

    @Test
    public void startTaskTwiceRecordInList() {
        Record record = mock(Record.class);
        taskSpy.addRecord(record);
        taskManagerSpy.startTask(taskSpy);
        taskManagerSpy.startTask(taskSpy);
        assertEquals(2, taskSpy.getRecords().size());
        assertEquals(true, taskSpy.isOn());
    }

    @Test
    public void stopTask() {
        taskManagerSpy.startTask(taskSpy);
        taskManagerSpy.stopTask(taskSpy);
        assertEquals(false, taskSpy.isOn());
    }

    @Test
    public void stopTaskTwice() {
        taskManagerSpy.startTask(taskSpy);
        taskManagerSpy.stopTask(taskSpy);
        taskManagerSpy.stopTask(taskSpy);
        assertEquals(false, taskSpy.isOn());
    }

    @Test
    public void getOverallDurationNull() {
        assertEquals(0, taskManagerSpy.getOverallDuration(null));
    }

    @Test
    public void getOverallDurationWithoutRecords() {
        assertEquals(0, taskManagerSpy.getOverallDuration(new Task()));
    }

    @Test
    public void getOverallDurationWithOneRecord() {
        long expected = 1000;
        Record record = mock(Record.class);
        taskSpy.addRecord(record);
        when(recordManagerMock.getDuration(record)).thenReturn(expected);
        assertEquals(expected, taskManagerSpy.getOverallDuration(taskSpy));
    }

    @Test
    public void getOverallDurationMultipleRecords() {
        int taskCount = 3;
        int recordDuration = 1000;
        Task task = this.getTaskWithRecords(taskCount, recordDuration);
        long expected = taskCount * recordDuration;
        assertEquals(expected, taskManagerSpy.getOverallDuration(task));
    }

    @Test
    public void getOverallDurationManyRecords() {
        int taskCount = 100;
        long recordDuration = 5556547842L;
        Task task = this.getTaskWithRecords(taskCount, recordDuration);
        long expected = taskCount * recordDuration;
        assertEquals(expected, taskManagerSpy.getOverallDuration(task));
    }

    @Test
    public void getDurationInTimespanAllNull() {
        assertEquals(0, taskManagerSpy.getDurationInTimespan(null, null, null));
    }

    @Test
    public void getDurationInTimespanTimespanNull() {
        assertEquals(0, taskManagerSpy.getDurationInTimespan(taskSpy, null, null));
    }

    @Test
    public void getDurationInTimespanWithEndDate() {
        createTestRecords(taskSpy);

        calendar.set(2016, 11, 3);
        Date timespanEnd = calendar.getTime();

        assertEquals(2000, taskManagerSpy.getDurationInTimespan(taskSpy, null, timespanEnd));
    }

    @Test
    public void getDurationInTimespanWithStart() {
        createTestRecords(taskSpy);

        calendar.set(2016, 11, 3);
        Date timespanStart = calendar.getTime();

        assertEquals(1000, taskManagerSpy.getDurationInTimespan(taskSpy, timespanStart, null));
    }

    @Test
    public void getDurationInTimespanWithStartAndEnd() {
        createTestRecords(taskSpy);

        calendar.set(2016, 11, 1);
        Date timespanStart = calendar.getTime();

        calendar.set(2016, 11, 3);
        Date timespanEnd = calendar.getTime();

        assertEquals(1000, taskManagerSpy.getDurationInTimespan(taskSpy,
                timespanStart, timespanEnd));
    }

    private void createTestRecords(Task task) {
        Date start;
        Calendar calendar = Calendar.getInstance();

        when(recordManagerMock.getDuration(any(Record.class))).thenReturn(1000L);

        calendar.set(2016, 10, 5, 0, 0, 0);
        start = calendar.getTime();

        calendar.set(2016, 10, 5, 0, 0, 1);
        Date end = calendar.getTime();

        Record record = mock(Record.class);
        when(record.getStart()).thenReturn(start);
        when(record.getEnd()).thenReturn(end);

        task.addRecord(record);

        calendar.set(2016, 11, 2, 0, 0, 0);
        start = calendar.getTime();

        calendar.set(2016, 11, 2, 0, 0, 1);
        end = calendar.getTime();

        record = mock(Record.class);
        when(record.getStart()).thenReturn(start);
        when(record.getEnd()).thenReturn(end);

        task.addRecord(record);

        calendar.set(2016, 11, 5, 0, 0, 0);
        start = calendar.getTime();

        calendar.set(2016, 11, 5, 0, 0, 1);
        end = calendar.getTime();

        record = mock(Record.class);
        when(record.getStart()).thenReturn(start);
        when(record.getEnd()).thenReturn(end);

        task.addRecord(record);
    }

    private Task getTaskWithRecords(int count, long duration) {
        Task task = new Task();
        for (int i = 0; i < count; i++) {
            Record record = mock(Record.class);
            when(recordManagerMock.getDuration(record)).thenReturn(duration);
            task.addRecord(record);
        }
        return task;
    }
}


