package de.hdm.timemachine;

import com.squareup.otto.Bus;

import de.hdm.timemachine.factories.DateFactory;
import de.hdm.timemachine.factories.RecordFactory;
import de.hdm.timemachine.managers.RecordManager;
import de.hdm.timemachine.managers.TaskManager;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class TimeMachineTestApplication extends TimeMachineApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        dateFactory = new DateFactory();
        recordManager = new RecordManager(dateFactory);
        taskManager = new TaskManager(recordManager, new RecordFactory());
        mainBus = new Bus();

        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .name("test")
                // .initialData() to set up test data
                .build();
        Realm.setDefaultConfiguration(config);

        databaseConnection = new DatabaseConnection(getMainBus(), Realm.getInstance(config));
        taskList = new TaskList(taskManager, getMainBus(), getDatabaseConnection().getAllTasks());
    }
}
