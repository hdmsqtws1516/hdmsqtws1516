package de.hdm.timemachine;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

import android.support.test.InstrumentationRegistry;
import android.support.test.annotation.UiThreadTest;
import android.support.test.rule.UiThreadTestRule;
import android.support.test.runner.AndroidJUnit4;

import de.hdm.timemachine.events.TaskCreatedEvent;
import de.hdm.timemachine.events.TaskRemovedEvent;
import de.hdm.timemachine.models.Task;

import io.realm.Realm;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class DatabaseConnectionTest {
    private TimeMachineApplication application;
    private DatabaseConnection databaseConnection;
    private Realm databaseRealm;

    @Rule
    public UiThreadTestRule uiThreadTestRule = new UiThreadTestRule();

    /**
     * Setup up the test environment.
     */
    @Before
    public void setup() {
        application = (TimeMachineApplication) InstrumentationRegistry.getTargetContext()
                .getApplicationContext();
        databaseConnection = application.getDatabaseConnection();
        databaseRealm = databaseConnection.getDatabaseRealm();
        databaseRealm.beginTransaction();
        databaseRealm.deleteAll();
        databaseRealm.commitTransaction();
    }

    @Test
    @UiThreadTest
    public void createTask() throws Exception {
        assertEquals(0, databaseRealm.where(Task.class).count());

        TaskCreatedEvent event = new TaskCreatedEvent(new Task("Test"));
        databaseConnection.createTask(event);

        assertEquals(1, databaseRealm.where(Task.class).count());

        assertEquals(
                event.getTask().getName(),
                databaseRealm.where(Task.class).equalTo("name", "Test").findFirst().getName()
        );
    }

    @Test
    @UiThreadTest
    public void deleteTask() throws Exception {
        Task testTask = new Task("Test");

        // create test task
        databaseRealm.beginTransaction();
        testTask = databaseRealm.copyToRealm(testTask);
        databaseRealm.commitTransaction();

        assertEquals(1, databaseRealm.where(Task.class).count());

        TaskRemovedEvent event = new TaskRemovedEvent(testTask);
        databaseConnection.deleteTask(event);

        assertEquals(0, databaseRealm.where(Task.class).count());

        assertNull(
                databaseRealm.where(Task.class).equalTo("name", "Test").findFirst()
        );
    }

    @Test
    @UiThreadTest
    public void updateTask() throws Exception {
        Task testTask = new Task("Test");

        // create test task
        databaseRealm.beginTransaction();
        testTask = databaseRealm.copyToRealm(testTask);
        databaseRealm.commitTransaction();

        assertEquals(0, databaseRealm.where(Task.class).equalTo("on", true).count());

        databaseRealm.beginTransaction();
        testTask.setOn(true);
        databaseRealm.commitTransaction();

        assertEquals(1, databaseRealm.where(Task.class).equalTo("on", true).count());
    }
}
