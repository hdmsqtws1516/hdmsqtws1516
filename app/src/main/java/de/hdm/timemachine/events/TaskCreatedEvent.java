package de.hdm.timemachine.events;

import de.hdm.timemachine.models.Task;

public class TaskCreatedEvent {
    private final Task task;

    public TaskCreatedEvent(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return task;
    }
}
