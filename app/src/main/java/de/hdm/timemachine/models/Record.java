package de.hdm.timemachine.models;

import io.realm.RealmObject;

import java.util.Date;

public class Record extends RealmObject {
    private Date start;
    private Date end;

    public Record() {}

    public Date getStart() throws NullPointerException {
        return (this.start != null) ? new Date(this.start.getTime()) : null;
    }

    public void setStart(Date start) {
        this.start = new Date(start.getTime());
    }

    public Date getEnd() {
        return (this.end != null) ? new Date(this.end.getTime()) : null;
    }

    public void setEnd(Date end) {
        this.end = new Date(end.getTime());

    }
}
