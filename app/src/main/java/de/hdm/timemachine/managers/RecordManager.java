package de.hdm.timemachine.managers;

import de.hdm.timemachine.factories.DateFactory;
import de.hdm.timemachine.models.Record;

public class RecordManager {
    private DateFactory dateFactory;

    public RecordManager(DateFactory dateFactory) {
        this.dateFactory = dateFactory;
    }

    public void startRecord(Record record) {
        record.setStart(dateFactory.getDate());
    }

    public void stopRecord(Record record) {
        record.setEnd(dateFactory.getDate());
    }

    /**
     * Returns the duration of the given record.
     *
     * @param record the record
     * @return duration (long)
     */
    public long getDuration(Record record) {
        long recordDuration = 0;

        if (record != null) {
            if (record.getStart() != null) {
                long end = getDurationEndTime(record);
                recordDuration = end - record.getStart().getTime();
            }
        }
        return recordDuration;
    }

    /**
     * Returns the end time of the given record.
     *
     * @param record the record
     * @return end time (long)
     */
    public long getDurationEndTime(Record record) {
        long end = 0;
        if (record != null) {
            if (record.getEnd() == null) {
                end = dateFactory.getDate().getTime();
            } else {
                end = record.getEnd().getTime();
            }
        }
        return end;
    }
}
