package de.hdm.timemachine.integration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.github.mikephil.charting.data.PieEntry;
import com.squareup.otto.Bus;

import de.hdm.timemachine.TaskList;
import de.hdm.timemachine.factories.DateFactory;
import de.hdm.timemachine.factories.PieChartDataFactory;
import de.hdm.timemachine.factories.RecordFactory;
import de.hdm.timemachine.managers.RecordManager;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.misc.PieChartDataHelper;
import de.hdm.timemachine.models.Record;
import de.hdm.timemachine.models.Task;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Michael on 19.02.2017.
 */

public class PieChartDataHelperTest {
    private TaskList taskListSpy;
    private TaskManager taskManagerSpy;
    private RecordManager recordManagerSpy;
    private Date currentDate;
    private DateFactory dateFactoryMock;
    private Bus eventBusSpy;
    private RecordFactory recordFactoryMock;
    private PieChartDataHelper pieChartDataHelper;
    private PieChartDataFactory pieChartDataFactorySpy;

    /**
     * Setup up the test environment.
     */
    @Before
    public void setup() {
        this.currentDate = new Date();
        dateFactoryMock = mock(DateFactory.class);
        doReturn(this.currentDate).when(dateFactoryMock).getDate();
        recordManagerSpy = spy(new RecordManager(dateFactoryMock));
        recordFactoryMock = mock(RecordFactory.class);
        when(recordFactoryMock.getRecord()).thenReturn(new Record());
        taskManagerSpy = spy(new TaskManager(recordManagerSpy, recordFactoryMock));
        eventBusSpy = spy(new Bus());
        taskListSpy = spy(new TaskList(taskManagerSpy, eventBusSpy));
        pieChartDataFactorySpy = new PieChartDataFactory();
        pieChartDataHelper = new PieChartDataHelper(taskManagerSpy, pieChartDataFactorySpy);
    }

    @Test
    public void getEntriesOneTaskOneRecord() {

        Task task = new Task("Test-Task");
        Record rec = spy(new Record());
        when(rec.getStart()).thenReturn(new Date(1234));
        when(rec.getEnd()).thenReturn(new Date(2000));
        List<Task> tasks = new ArrayList<>();
        task.addRecord(rec);
        tasks.add(task);
        Date startDateTimespan = new Date(0);
        Date endDateTimespan = new Date(3000);
        List<PieEntry> entries;
        entries = pieChartDataHelper.getEntries(tasks, startDateTimespan, endDateTimespan);
        Long dur = taskManagerSpy.getDurationInTimespan(task, startDateTimespan, endDateTimespan);
        PieEntry exeptedEntry = pieChartDataFactorySpy.createPieEntry(dur,task.getName());
        assertEquals(1, entries.size());
        assertEquals(task.getName(), entries.get(0).getLabel());
        assertEquals(dur, (entries.get(0).getValue()), 0.00);
    }
}
