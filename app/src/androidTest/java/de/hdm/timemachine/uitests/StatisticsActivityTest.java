package de.hdm.timemachine.uitests;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.runner.lifecycle.Stage.RESUMED;

import android.app.Activity;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.widget.DatePicker;

import com.github.mikephil.charting.charts.PieChart;
import de.hdm.timemachine.R;
import de.hdm.timemachine.TimeMachineApplication;
import de.hdm.timemachine.Transactional;
import de.hdm.timemachine.activies.StatisticsActivity;
import de.hdm.timemachine.models.Record;
import de.hdm.timemachine.models.Task;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;
import java.util.Collection;

/**
 * Instrumented UI-tests for the {@link StatisticsActivity}.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class StatisticsActivityTest {

    /**
     *  Set up data before running the tests.
     *
     */
    @BeforeClass
    @Transactional
    public static void setUp() {
        Calendar cal = Calendar.getInstance();

        // add dummy Tasks & Records
        Task task2 = new Task("test task 2");

        Record record1 = new Record();

        cal.set(2017, 0, 1, 12, 00, 00);
        record1.setStart(cal.getTime());

        cal.add(Calendar.SECOND, 2);
        record1.setEnd(cal.getTime());

        Record record2 = new Record();
        Record record3 = new Record();
        Record record4 = new Record();
        Record record5 = new Record();

        Task task1 = new Task("test task 1");
        task1.addRecord(record1);

        TimeMachineApplication application = (TimeMachineApplication) getInstrumentation()
                .getTargetContext().getApplicationContext();
        application.getDatabaseRealm().copyToRealm(task1);

        application.getTaskList().addTask(task1);
    }

    /**
     *  Clean up the database after each test.
     */
    @After
    @Transactional
    public void cleanUp() {
        TimeMachineApplication application = (TimeMachineApplication) getInstrumentation()
                .getTargetContext().getApplicationContext();
        application.getDatabaseRealm().deleteAll();
    }

    @Rule
    public IntentsTestRule<StatisticsActivity> statisticsActivityRule =
            new IntentsTestRule<>(StatisticsActivity.class);


    @Test
    public void testDatePickerFromSetDate() {
        onView(withId(R.id.et_from)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
                .check(matches(isDisplayed()));

        int year = 2020;
        int month = 11;
        int day = 15;

        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
                .perform(PickerActions.setDate(year, month, day));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.et_from)).check(matches(withText(day + "/" + month + "/" + year)));
    }


    @Test
    public void testDatePickerToSetDate() {
        onView(withId(R.id.et_to)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
                .check(matches(isDisplayed()));

        int year = 2010;
        int month = 10;
        int day = 14;

        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
                .perform(PickerActions.setDate(year, month, day));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.et_to)).check(matches(withText(day + "/" + month + "/" + year)));
    }

    private Activity getCurrentActivity() {
        final Activity[] currentActivity = new Activity[1];
        getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                Collection resumedActivities = ActivityLifecycleMonitorRegistry.getInstance()
                        .getActivitiesInStage(RESUMED);
                if (resumedActivities.iterator().hasNext()) {
                    currentActivity[0] = (Activity)resumedActivities.iterator().next();
                }
            }
        });

        return currentActivity[0];
    }

    private PieChart getPieChart() {
        StatisticsActivity activity = (StatisticsActivity) getCurrentActivity();
        return (PieChart) activity.findViewById(R.id.chart);
    }
}
