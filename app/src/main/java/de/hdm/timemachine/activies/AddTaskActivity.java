package de.hdm.timemachine.activies;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import de.hdm.timemachine.R;
import de.hdm.timemachine.TaskList;
import de.hdm.timemachine.TimeMachineApplication;
import de.hdm.timemachine.Transactional;
import de.hdm.timemachine.factories.TaskFactory;
import de.hdm.timemachine.models.Task;


/**
 * A screen to create a new Task.
 */
public class AddTaskActivity extends AppCompatActivity implements OnClickListener {
    private TaskList taskList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        taskList = ((TimeMachineApplication)getApplication()).getTaskList();

        Button addTaskButton = (Button) findViewById(R.id.bn_create_task);
        addTaskButton.setOnClickListener(this);
    }

    @Override
    @Transactional
    public void onClick(View view) {
        EditText name = (EditText) findViewById(R.id.name);
        Task newTask = new TaskFactory().getTask(name.getText().toString());
        taskList.addTask(newTask);

        finish();
    }
}

