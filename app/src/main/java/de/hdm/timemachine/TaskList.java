package de.hdm.timemachine;

import android.support.annotation.NonNull;

import com.squareup.otto.Bus;

import de.hdm.timemachine.events.TaskRemovedEvent;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.models.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

public class TaskList extends Observable {
    private final Bus bus;
    private ArrayList<Task> tasks = new ArrayList<>();
    private TaskManager taskManager;

    public TaskList(@NonNull TaskManager taskManager, @NonNull Bus eventBus) {
        this.taskManager = taskManager;
        this.bus = eventBus;
    }

    public TaskList(TaskManager taskManager, Bus eventBus, Task[] tasks) {
        this(taskManager, eventBus);
        this.tasks.addAll(Arrays.asList(tasks));
    }

    /**
     * Adds the given task to the {@link TaskList}.
     *
     * @param task the {@link Task} to add
     */
    public void addTask(Task task) {
        if (task != null) {
            tasks.add(task);
            setChanged();
            notifyObservers();
        }
    }

    /**
     * Removes the given task from list.
     *
     * @param task that should be removed
     */
    public void removeTask(Task task) {
        if (task != null) {
            tasks.remove(task);
            setChanged();
            notifyObservers();
            bus.post(new TaskRemovedEvent(task));
        }
    }

    /**
     * Sets the given Task to the given state.
     *
     * @param task   that should be changed
     * @param active boolen
     */
    @Transactional
    public void setTaskActive(Task task, boolean active) {
        if (task != null) {
            if (active) {
                taskManager.startTask(task);
            } else {
                taskManager.stopTask(task);
            }
            setChanged();
            notifyObservers();
        }
    }

    public List<Task> getAllTasks() {
        return this.tasks;
    }
}

