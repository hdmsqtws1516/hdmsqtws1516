package de.hdm.timemachine.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import de.hdm.timemachine.TaskList;
import de.hdm.timemachine.models.Task;

import java.util.Observable;
import java.util.Observer;

public class TaskAdapter extends ArrayAdapter<Task> implements Observer {
    private final TaskList taskList;
    private LayoutInflater inflater;

    /**
     * Constructor.
     *
     * @param context Adapter Context
     */
    public TaskAdapter(Context context, TaskList taskList) {
        super(context, android.R.layout.simple_list_item_1);
        this.taskList = taskList;
        this.addAll(taskList.getAllTasks());
        this.taskList.addObserver(this);
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        Task task = getItem(position);

        if (task != null && task.isValid()) {
            holder.name.setText(task.getName());
            if (task.isOn()) {
                view.setBackgroundColor(Color.GREEN);
            } else {
                view.setBackgroundColor(Color.WHITE);
            }
        }

        return view;
    }

    @Override
    public void update(Observable observable, Object data) {
        this.clear();
        this.addAll(taskList.getAllTasks());
        this.notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(android.R.id.text1)
        TextView name;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
