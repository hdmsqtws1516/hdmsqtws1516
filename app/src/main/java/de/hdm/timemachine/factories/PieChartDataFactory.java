package de.hdm.timemachine.factories;

import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;

public class PieChartDataFactory {

    /**
     * Creates a pie entry.
     *
     * @param value the value of the entry
     * @param name  the name of the entry
     * @return PieEntry Object
     */
    public PieEntry createPieEntry(long value, String name) {
        PieEntry entry = new PieEntry(value);
        entry.setLabel(name);
        return entry;
    }

    /**
     * Creates a PieDataSet.
     *
     * @param name    the name of the PieDataSet
     * @param entries List of PieEntries
     * @param colors  List of colors
     * @return PieDataSet Object
     */
    public PieDataSet createPieDataSet(String name, List<PieEntry> entries,
                                       ArrayList<Integer> colors) {
        PieDataSet dataSet = new PieDataSet(entries, name);
        dataSet.setColors(colors);
        return dataSet;
    }

    public PieData createPieData(PieDataSet dataSet) {
        return new PieData(dataSet);
    }


}
