package de.hdm.timemachine.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.squareup.otto.Bus;

import de.hdm.timemachine.TaskList;
import de.hdm.timemachine.events.TaskRemovedEvent;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.models.Task;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class TaskListTest {

    private TaskManager taskManagerMock;
    private TaskList taskListSpy;
    private Bus eventBusMock;

    /**
     * Setup up the test environment.
     */
    @Before
    public void initialize() {
        taskManagerMock = mock(TaskManager.class);
        eventBusMock = mock(Bus.class);
        taskListSpy = spy(new TaskList(taskManagerMock, eventBusMock));
    }

    @Test
    public void constructorTaskList() {
        Task[] task = new Task[100];
        TaskList taskList = new TaskList(taskManagerMock, eventBusMock, task);
        assertEquals(100, taskList.getAllTasks().size());
    }

    @Test
    public void initTaskList() {
        assertTrue(taskListSpy.getAllTasks().isEmpty());
    }

    @Test
    public void addTaskNull() {
        taskListSpy.addTask(null);
        int expects = 0;
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void addTaskOne() {
        Task task = mock(Task.class);
        taskListSpy.addTask(task);
        int expects = 1;
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void addTasksMultiple() {
        int expects = 3;
        fillListWithTasks(taskListSpy, expects);
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void addTasksMany() {
        int expects = 100;
        fillListWithTasks(taskListSpy, expects);
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void removeTaskWithNull() {
        int expects = 5;
        fillListWithTasks(taskListSpy, expects);
        taskListSpy.removeTask(null);
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void removeOneTaskFromList() {
        int tasksCount = 5;
        fillListWithTasks(taskListSpy, tasksCount);
        int taskIndexToRemove = Math.round(tasksCount / 2);
        taskListSpy.removeTask(taskListSpy.getAllTasks().get(taskIndexToRemove));
        int expects = tasksCount - 1;
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void removeOneTaskRemovedEvent() {
        fillListWithTasks(taskListSpy, 1);
        taskListSpy.removeTask(taskListSpy.getAllTasks().get(0));
        verify(eventBusMock).post(isA(TaskRemovedEvent.class));
    }

    @Test
    public void removeFirstTaskFromList() {
        int tasksCount = 5;
        fillListWithTasks(taskListSpy, tasksCount);
        taskListSpy.removeTask(taskListSpy.getAllTasks().get(0));
        int expects = tasksCount - 1;
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void removeLastTaskFromList() {
        int tasksCount = 5;
        fillListWithTasks(taskListSpy, tasksCount);
        taskListSpy.removeTask(taskListSpy.getAllTasks().get(tasksCount - 1));
        int expects = tasksCount - 1;
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void removeManyTasksFromList() {
        int tasksCount = 100;
        int tasksCountToRemove = 50;
        fillListWithTasks(taskListSpy, tasksCount);
        removeTasksFromList(taskListSpy, tasksCountToRemove);
        int expects = tasksCount - tasksCountToRemove;
        int actual = taskListSpy.getAllTasks().size();
        assertEquals(expects, actual);
    }

    @Test
    public void setTaskActiveNull() {
        taskListSpy.setTaskActive(null, true);
        verify(taskManagerMock, times(0)).startTask(null);
    }

    @Test
    public void setTaskNotActiveNull() {
        taskListSpy.setTaskActive(null, false);
        verify(taskManagerMock, times(0)).stopTask(null);
    }

    @Test
    public void setTaskActive() {
        Task task = mock(Task.class);
        taskListSpy.setTaskActive(task, true);
        verify(taskManagerMock, times(1)).startTask(task);
    }

    @Test
    public void setTaskNotActive() {
        Task task = mock(Task.class);
        taskListSpy.setTaskActive(task, false);
        verify(taskManagerMock, times(1)).stopTask(task);
    }

    private void removeTasksFromList(TaskList taskList, int count) {
        for (int i = 0; i < count; i++) {
            taskList.removeTask(taskList.getAllTasks().get(i));
        }
    }

    private void fillListWithTasks(TaskList taskList, int count) {
        for (int i = 0; i < count; i++) {
            Task task = mock(Task.class);
            taskList.addTask(task);
        }
        resetBus();
    }

    private void resetBus() {
        Mockito.reset(eventBusMock);
    }
}

