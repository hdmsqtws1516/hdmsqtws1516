package de.hdm.timemachine.managers;

import de.hdm.timemachine.factories.RecordFactory;
import de.hdm.timemachine.models.Record;
import de.hdm.timemachine.models.Task;

import java.util.Date;
import java.util.List;

public class TaskManager {
    private RecordManager recordManager;
    private final RecordFactory recordFactory;

    public TaskManager(RecordManager recordManager, RecordFactory recordFactory) {
        this.recordManager = recordManager;
        this.recordFactory = recordFactory;
    }

    /**
     * Checks if a Task has Records.
     *
     * @param task the Task
     * @return boolean
     */
    public boolean hasRecords(Task task) {
        boolean hasRecords = false;
        if (task != null) {
            hasRecords = !task.getRecords().isEmpty();
        }
        return hasRecords;
    }

    /**
     * Adds a Record to a given Task.
     *
     * @param task   the Task
     * @param record the Record
     */
    public void addRecordToTask(Task task, Record record) {
        task.setActiveRecord(record);
        task.addRecord(record);
    }

    /**
     * Starts a Task.
     *
     * @param task Task Object
     */
    public void startTask(Task task) {
        if (!task.isOn()) {
            Record record = recordFactory.getRecord();
            recordManager.startRecord(record);
            this.addRecordToTask(task, record);
            task.setOn(true);
        }
    }

    /**
     * Stops a Task.
     *
     * @param task Task Object
     */
    public void stopTask(Task task) {
        if (task.isOn()) {
            recordManager.stopRecord(task.getActiveRecord());
            task.setOn(false);
        }
    }

    /**
     * Calc the overall duration of the tasks.
     *
     * @param task Task Object
     * @return long overall Duration
     */
    public long getOverallDuration(Task task) {
        long overallDuration = 0;
        if (task != null) {
            List<Record> records = task.getRecords();
            for (int i = 0; i < records.size(); i++) {
                overallDuration += recordManager.getDuration(records.get(i));
            }
        }
        return overallDuration;
    }

    /**
     * Returns the duration of the given {@link Task} during the given timespan.
     *
     * @param task          the task to get the duration of
     * @param timespanStart start of the timespan
     * @param timespanEnd   end of the timespan
     * @return long the duration of the given {@link Task} during the given timespan
     */
    public long getDurationInTimespan(Task task, Date timespanStart, Date timespanEnd) {
        if (task == null) {
            return 0;
        }

        long durationInTimespan = 0;
        for (Record r : task.getRecords()) {
            if ((timespanStart == null || r.getStart().after(timespanStart))
                    && (timespanEnd == null || r.getEnd().before(timespanEnd))) {
                durationInTimespan += recordManager.getDuration(r);
            }
        }

        return durationInTimespan;
    }
}
