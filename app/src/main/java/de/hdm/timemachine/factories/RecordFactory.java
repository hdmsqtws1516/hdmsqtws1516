package de.hdm.timemachine.factories;

import de.hdm.timemachine.models.Record;

import io.realm.Realm;

public class RecordFactory {
    public Record getRecord() {
        return Realm.getDefaultInstance().createObject(Record.class);
    }
}
