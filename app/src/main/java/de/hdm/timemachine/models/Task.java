package de.hdm.timemachine.models;

import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.List;

public class Task extends RealmObject {

    private String name;
    private RealmList<Record> records = new RealmList<>();

    private boolean on = false;
    private Record activeRecord;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public boolean isOn() {
        return this.on;
    }

    public String getName() {
        return this.name;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setActiveRecord(Record record) {
        this.activeRecord = record;
    }

    public Record getActiveRecord() {
        return activeRecord;
    }

    /**
     * Add a Record to the Record list.
     *
     * @param newRecord the Record
     */
    public void addRecord(Record newRecord) {
        if (newRecord != null) {
            this.records.add(newRecord);
        }
    }

    @Override
    public String toString() {
        return "Task{"
                + ", name='" + name + '\''
                + ", records=" + records
                + ", on=" + on
                + ", activeRecord=" + activeRecord
                + '}';
    }
}
