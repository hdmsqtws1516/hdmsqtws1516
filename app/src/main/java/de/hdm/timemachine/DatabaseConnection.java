package de.hdm.timemachine;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import de.hdm.timemachine.events.TaskCreatedEvent;
import de.hdm.timemachine.events.TaskRemovedEvent;
import de.hdm.timemachine.models.Task;

import io.realm.Realm;
import io.realm.RealmResults;

public class DatabaseConnection {
    private final Realm databaseRealm;

    /**
     * An instance of this class reacts to events on the given event bus by synchronizing
     * model changes to the database.
     *
     * @param eventBus the event bus to listen for events on.
     * @param databaseRealm the database to operate on.
     */
    DatabaseConnection(Bus eventBus, Realm databaseRealm) {
        this.databaseRealm = databaseRealm;
        assert eventBus != null && databaseRealm != null;

        eventBus.register(this);
    }

    public Realm getDatabaseRealm() {
        return databaseRealm;
    }

    /**
     * Get all tasks from database.
     * @return RealmResults
     */
    public Task[] getAllTasks() {
        RealmResults<Task> tasks = databaseRealm.where(Task.class).findAll();
        return tasks.toArray(new Task[tasks.size()]);
    }

    /**
     * This method is invoked every time a {@link TaskCreatedEvent} is posted on the bus,
     * and saves the created {@link Task} to the database.
     *
     * @param event a {@link TaskCreatedEvent}
     */
    @Subscribe
    public void createTask(final TaskCreatedEvent event) {
        databaseRealm.beginTransaction();
        databaseRealm.insert(event.getTask());
        databaseRealm.commitTransaction();
    }

    /**
     * This method is invoked every time a {@link TaskRemovedEvent} is posted on the bus,
     * and removes the {@link Task} from the database.
     *
     * @param event a {@link TaskCreatedEvent}
     */
    @Subscribe
    public void deleteTask(final TaskRemovedEvent event) {
        databaseRealm.beginTransaction();
        event.getTask().deleteFromRealm();
        databaseRealm.commitTransaction();
    }
}
