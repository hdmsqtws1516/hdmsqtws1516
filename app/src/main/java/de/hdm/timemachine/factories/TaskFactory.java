package de.hdm.timemachine.factories;

import de.hdm.timemachine.models.Task;
import io.realm.Realm;

public class TaskFactory {
    public Task getTask(String name) {
        return Realm.getDefaultInstance().copyToRealm(new Task(name));
    }
}
