package de.hdm.timemachine;

import android.app.Application;

import com.squareup.otto.Bus;

import de.hdm.timemachine.factories.DateFactory;
import de.hdm.timemachine.factories.RecordFactory;
import de.hdm.timemachine.managers.RecordManager;
import de.hdm.timemachine.managers.TaskManager;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class TimeMachineApplication extends Application {
    TaskList taskList;
    TaskManager taskManager;
    RecordManager recordManager;
    DateFactory dateFactory;
    Bus mainBus;
    DatabaseConnection databaseConnection;

    @Override
    public void onCreate() {
        super.onCreate();
        dateFactory = new DateFactory();
        recordManager = new RecordManager(dateFactory);
        taskManager = new TaskManager(recordManager, new RecordFactory());
        mainBus = new Bus();

        initializeDatabase();

        taskList = new TaskList(taskManager, getMainBus(), getDatabaseConnection().getAllTasks());
    }

    public TaskList getTaskList() {
        return taskList;
    }

    public TaskManager getTaskManager() {
        return taskManager;
    }

    public RecordManager getRecordManager() {
        return recordManager;
    }

    public DateFactory getDateFactory() {
        return dateFactory;
    }

    public Bus getMainBus() {
        return mainBus;
    }

    public DatabaseConnection getDatabaseConnection() {
        return databaseConnection;
    }

    /**
     *  Initializes the database.
     */
    public void initializeDatabase() {
        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        databaseConnection = new DatabaseConnection(getMainBus(), getDatabaseRealm());
    }

    public Realm getDatabaseRealm() {
        return Realm.getDefaultInstance();
    }
}
