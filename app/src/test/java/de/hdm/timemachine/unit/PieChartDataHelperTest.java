package de.hdm.timemachine.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.github.mikephil.charting.data.PieEntry;

import de.hdm.timemachine.factories.PieChartDataFactory;
import de.hdm.timemachine.factories.RecordFactory;
import de.hdm.timemachine.managers.RecordManager;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.misc.PieChartDataHelper;
import de.hdm.timemachine.models.Record;
import de.hdm.timemachine.models.Task;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PieChartDataHelperTest {
    private TaskManager taskManagerSpy;
    private RecordManager recordManagerMock = mock(RecordManager.class);
    private RecordFactory recordFactoryMock;
    private PieChartDataHelper pieChartDataHelper;
    private Calendar calendar;
    private PieChartDataFactory pieChartDataFactoryMock;
    private Date timespanStart;
    private Date timespanEnd;

    /**
     * Setup up the test environment.
     */
    @Before
    public void setup() {
        recordFactoryMock = mock(RecordFactory.class);
        when(recordFactoryMock.getRecord()).thenReturn(new Record());
        taskManagerSpy = mock(TaskManager.class);

        pieChartDataFactoryMock = mock(PieChartDataFactory.class);
        pieChartDataHelper = spy(new PieChartDataHelper(taskManagerSpy, pieChartDataFactoryMock));
        calendar = Calendar.getInstance();

        calendar.set(2016, 11, 1);
        this.timespanStart = calendar.getTime();

        calendar.set(2016, 11, 3);
        this.timespanEnd = calendar.getTime();
    }

    @Test
    public void getEntriesTaskListNull() {
        List<PieEntry> entries = pieChartDataHelper.getEntries(null, timespanStart, timespanEnd);
        assertEquals(0, entries.size());
    }

    @Test
    public void getEntriesTaskListEmpty() {
        List<Task> tasks = new ArrayList<Task>();
        List<PieEntry> entries = pieChartDataHelper.getEntries(tasks, timespanStart, timespanEnd);
        assertEquals(0, entries.size());
    }

    @Test
    public void getEntriesOneTaskNoTime() {
        List<Task> tasks = this.getTaskMocks(1, false);
        List<PieEntry> entries = pieChartDataHelper.getEntries(tasks, timespanStart, timespanEnd);
        assertEquals(0, entries.size());
    }

    @Test
    public void getEntriesManyTaskNoTime() {
        List<Task> tasks = this.getTaskMocks(100, false);
        List<PieEntry> entries = pieChartDataHelper.getEntries(tasks, timespanStart, timespanEnd);
        assertEquals(0, entries.size());
    }


    @Test
    public void getEntriesOneTaskWithTime() {
        List<Task> tasks = this.getTaskMocks(1, true);
        List<PieEntry> entries = pieChartDataHelper.getEntries(tasks, timespanStart, timespanEnd);
        assertEquals(1, entries.size());
    }

    @Test
    public void getEntriesMultipleTaskWithTime() {
        List<Task> tasks = this.getTaskMocks(3, true);
        List<PieEntry> entries = pieChartDataHelper.getEntries(tasks, timespanStart, timespanEnd);
        assertEquals(3, entries.size());
    }

    @Test
    public void getEntriesManyTaskWithTime() {
        List<Task> tasks = this.getTaskMocks(100, true);
        List<PieEntry> entries = pieChartDataHelper.getEntries(tasks, timespanStart, timespanEnd);
        assertEquals(100, entries.size());
    }

    private List<Task> getTaskMocks(int count, boolean withTime) {
        List<Task> tasks = new ArrayList<>();
        long time = 100;
        for (int i = 0; i < count; i++) {
            Task task = mock(Task.class);
            if (withTime) {
                when(taskManagerSpy.getDurationInTimespan(task,
                        this.timespanStart, this.timespanEnd)).thenReturn(time);
            }
            tasks.add(task);
        }
        return tasks;
    }
}


