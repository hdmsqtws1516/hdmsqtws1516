package de.hdm.timemachine.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.squareup.otto.Bus;

import de.hdm.timemachine.TaskList;
import de.hdm.timemachine.factories.DateFactory;
import de.hdm.timemachine.factories.RecordFactory;
import de.hdm.timemachine.managers.RecordManager;
import de.hdm.timemachine.managers.TaskManager;
import de.hdm.timemachine.models.Record;
import de.hdm.timemachine.models.Task;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

/**
 * Unit Test for the RecordManager and a Record.
 */
public class TaskListTest {

    private TaskList taskListSpy;
    private TaskManager taskManagerSpy;
    private RecordManager recordManagerSpy;
    private Date currentDate;
    private DateFactory dateFactoryMock;
    private Bus eventBusSpy;
    private RecordFactory recordFactoryMock;

    /**
     * Setup up the test environment.
     */
    @Before
    public void setup() {
        this.currentDate = new Date();
        dateFactoryMock = mock(DateFactory.class);
        doReturn(this.currentDate).when(dateFactoryMock).getDate();
        recordManagerSpy = spy(new RecordManager(dateFactoryMock));
        recordFactoryMock = mock(RecordFactory.class);
        when(recordFactoryMock.getRecord()).thenReturn(new Record());
        taskManagerSpy = spy(new TaskManager(recordManagerSpy, recordFactoryMock));
        eventBusSpy = spy(new Bus());
        taskListSpy = spy(new TaskList(taskManagerSpy, eventBusSpy));
    }

    @Test
    public void setTaskActiveTest() {
        Task task = new Task("Test-Task");
        taskListSpy.setTaskActive(task, true);
        assertEquals(true, task.isOn());
        assertNotNull(task.getActiveRecord().getStart());
        assertNull(task.getActiveRecord().getEnd());
    }

    @Test
    public void setRunningTaskActiveAgainTest() {
        Task task = new Task("Test-Task");
        taskListSpy.setTaskActive(task, true); // first Time start
        taskListSpy.setTaskActive(task, true); // second Time start
        assertEquals(true, task.isOn());
        assertNotNull(task.getActiveRecord().getStart());
        assertNull(task.getActiveRecord().getEnd());
    }

    @Test
    public void setRunningTaskNotActiveTest() {
        Task task = new Task("Test-Task");
        taskListSpy.setTaskActive(task, true); // run task
        taskListSpy.setTaskActive(task, false);
        assertEquals(false, task.isOn());
        assertNotNull(task.getActiveRecord().getStart());
        assertNotNull(task.getActiveRecord().getEnd());
    }

    @Test
    public void setNotRunningTaskNotActiveTest() {
        Task task = new Task("Test-Task");
        taskListSpy.setTaskActive(task, false);
        assertEquals(false, task.isOn());
        assertNull(task.getActiveRecord());
    }

    // stop running task
}

